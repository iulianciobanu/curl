<?php

namespace Cortexica\Curl;

/**
 * Class Curl
 * @package Cortexica\Curl
 */
class Curl
{
    /**
     * @var string
     */
    private $url;
    /**
     * @var
     */
    private $path;
    /**
     * @var bool|array
     */
    private $post = false;
    /**
     * @var bool|array
     */
    private $headers = false;
    /**
     * @var mixed
     */
    private $response;
    /**
     * @var bool|integer
     */
    public $status;

    /**
     * @var mixed
     */
    public $error;

    /**
     * Curl constructor.
     *
     * @param string $baseUrl
     * @param string $path
     */
    public function __construct(string $baseUrl = null, string $path = null)
    {
        $this->url = $baseUrl;
        $this->path = $path;
    }

    /**
     * @param string $url
     *
     * @return Curl
     */
    public function setUrl(string $url): Curl
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @param string $path
     *
     * @return Curl
     */
    public function setPath(string $path): Curl
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @param array $postFields
     *
     * @return Curl
     */
    public function setPost(array $postFields): Curl
    {
        $this->post = $postFields;

        return $this;
    }

    /**
     * @param string $content
     *
     * @return Curl
     */
    public function setRaw(string $content): Curl
    {
        $this->post = $content;

        return $this;
    }

    /**
     * @param array $headers
     *
     * @return Curl
     */
    public function setHeaders(array $headers): Curl
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * @param array $default
     *
     * @return array
     */
    public function json(array $default = []): array
    {
        $response = $this->exec();
        if ($this->status === 200) {
            return json_decode($response, true);
        }

        return $default;
    }

    /**
     * @return string
     */
    public function exec(): string
    {
        $this->_exec();

        return $this->response;
    }

    /**
     *
     */
    private function _exec()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url . $this->path);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYSTATUS, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, false);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);

        if ($this->post) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $this->post);
        }

        if ($this->headers) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        }

        $this->response = curl_exec($ch);
        $this->status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $this->error = curl_error($ch);

        curl_close($ch);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->exec();
    }
}